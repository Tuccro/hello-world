import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by tuccro on 12/5/15.
 */
public class GreetingMessageCreator {

    final static Logger logger = Logger.getLogger(GreetingMessageCreator.class);

    public static final int BEGIN_OF_MORNING = 6;
    public static final int BEGIN_OF_DAY = 9;
    public static final int BEGIN_OF_EVENING = 19;
    public static final int BEGIN_OF_NIGHT = 23;
    private static final String GREETINGS_MESSAGES_FILE_NAME = "GreetingsMessages";
    private static final String MESSAGE_MORNING = "morning";
    private static final String MESSAGE_DAY = "day";
    private static final String MESSAGE_EVENING = "evening";
    private static final String MESSAGE_NIGHT = "night";

    private CurrentLocalTime localTime;

    public GreetingMessageCreator(CurrentLocalTime localTime) {

        this.localTime = localTime;
    }

    public String getGreetingMessage() throws IllegalArgumentException, NullPointerException {

        if (localTime == null) {
            logger.error("Illegal LocalTime: Localtime can't be null");
            throw new IllegalArgumentException();
        }

        logger.info("User's time is " + localTime.toString());

        int hours = localTime.getHour();

        try {
            return getGreetingResourceByTime(hours);
        } catch (MissingResourceException e) {
            logger.error("Error: application resources interrupted");
            throw new NullPointerException();
        } catch (IllegalArgumentException e){
            logger.error("LocalTime isn't valid");
            throw new IllegalArgumentException();
        }
    }

    private String getGreetingResourceByTime(int hours) throws MissingResourceException, IllegalArgumentException {

        String message;
        ResourceBundle messagesBundle = ResourceBundle.getBundle(GREETINGS_MESSAGES_FILE_NAME);

        if (hours >= BEGIN_OF_MORNING
                && hours < BEGIN_OF_DAY) {
            message = messagesBundle.getString(MESSAGE_MORNING);
        } else if (hours >= BEGIN_OF_DAY
                && hours < BEGIN_OF_EVENING) {
            message = messagesBundle.getString(MESSAGE_DAY);
        } else if (hours >= BEGIN_OF_EVENING
                && hours < BEGIN_OF_NIGHT) {
            message = messagesBundle.getString(MESSAGE_EVENING);
        } else if (hours == BEGIN_OF_NIGHT
                || hours < BEGIN_OF_MORNING) {
            message = messagesBundle.getString(MESSAGE_NIGHT);
        } else {
            throw new IllegalArgumentException();
        }

        try {
            message = new String(message.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error("Unsupported encoding of string resources");
        }

        logger.info("Greeting message: " + message);
        return message;
    }
}
