import org.apache.log4j.Logger;

/**
 * Created by tuccro on 12/5/15.
 */
public class Main {

    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {

        CurrentLocalTime localTime = new CurrentLocalTime();

        GreetingMessageCreator greetingMessageCreator = new GreetingMessageCreator(localTime);

        try {
            System.out.println(greetingMessageCreator.getGreetingMessage());
        } catch (IllegalArgumentException | NullPointerException e) {
            logger.error("Error occured while getting greeting message");
        }
    }
}
