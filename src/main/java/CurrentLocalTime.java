import java.time.LocalTime;

/**
 * Created by tuccro on 12/6/15.
 */
public class CurrentLocalTime {

    LocalTime localTime;

    public CurrentLocalTime() {
        localTime = LocalTime.now();
    }

    public int getHour() {
        return localTime.getHour();
    }

    @Override
    public String toString() {
        return localTime.toString();
    }
}
