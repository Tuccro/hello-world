import org.junit.runner.Description;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

/**
 * Created by tuccro on 12/6/15.
 */
public class StartTests {

    public static void main(String[] args) {
        JUnitCore core = new JUnitCore();
        core.addListener(new JUnitRunListener());

        core.run(GreetingsTest.class);
    }

    public static class JUnitRunListener extends RunListener {

        @Override
        public void testStarted(Description desc) {
            System.out.println("Started:" + desc.getDisplayName());
        }

        @Override
        public void testFinished(Description desc) {
            System.out.println("Finished:" + desc.getDisplayName());
        }

        @Override
        public void testFailure(Failure fail) {
            System.out.println("Failed:" + fail.getDescription().getDisplayName() + " [" + fail.getMessage() + "]");
        }
    }
}
