import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

/**
 * Created by tuccro on 12/6/15.
 */
public class GreetingsTest {

    @Test
    public void greetingMessageShouldReturnMorning() throws Exception {

        CurrentLocalTime morningTime = Mockito.mock(CurrentLocalTime.class);
        when(morningTime.getHour()).thenReturn(6);

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(morningTime);
        String message = messageCreator.getGreetingMessage();
        Assert.assertEquals("Good morning, World!", message);
    }

    @Test
    public void greetingMessageShouldReturnDay() throws Exception {

        CurrentLocalTime morningTime = Mockito.mock(CurrentLocalTime.class);
        when(morningTime.getHour()).thenReturn(9);

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(morningTime);
        String message = messageCreator.getGreetingMessage();
        Assert.assertEquals("Good day, World!", message);
    }

    @Test
    public void greetingMessageShouldReturnEvening() throws Exception {

        CurrentLocalTime morningTime = Mockito.mock(CurrentLocalTime.class);
        when(morningTime.getHour()).thenReturn(19);

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(morningTime);
        String message = messageCreator.getGreetingMessage();
        Assert.assertEquals("Good evening, World!", message);
    }

    @Test
    public void greetingMessageShouldReturnNight() throws Exception {

        CurrentLocalTime morningTime = Mockito.mock(CurrentLocalTime.class);
        when(morningTime.getHour()).thenReturn(23);

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(morningTime);
        String message = messageCreator.getGreetingMessage();
        Assert.assertEquals("Good night, World!", message);
    }

    @Test(expected = IllegalArgumentException.class)
    public void greetingMessageShouldThrowIllegalArgumentExceptionBecauseInvalidValueOfHours() throws Exception {

        CurrentLocalTime morningTime = Mockito.mock(CurrentLocalTime.class);
        when(morningTime.getHour()).thenReturn(25);

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(morningTime);
        messageCreator.getGreetingMessage();
    }

    @Test(expected = IllegalArgumentException.class)
    public void greetingMessageShouldThrowIllegalArgumentExceptionBecauseNull() throws Exception {

        GreetingMessageCreator messageCreator = new GreetingMessageCreator(null);
        messageCreator.getGreetingMessage();
    }
}
